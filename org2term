#!/usr/bin/env bash
# ----------------------------------------------------------------------------
# Script     : org2term
# Description: Org markup to escape sequences parser.
# Version    : 0.0.1
# Date       : 10/08/2022
# License    : GNU GPLv3.0+
# ----------------------------------------------------------------------------
# Copyright (C) 2022, Blau Araujo <blau@debxp.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
# ----------------------------------------------------------------------------
# Usage: org2term [OPTIONS] FILE
#        org2term [OPTIONS] < FILE
#        COMMAND | org2term [OPTIONS]
#        org2term -h|-v
# ----------------------------------------------------------------------------

version='0.0.1'

# help and version -----------------------------------------------------------

usage="Org markup to escape sequences parser (org2term) $version

USAGE:

Interactive mode

    org2term [OPTIONS] FILE
    org2term -h|-v

Non-interactive mode

    org2term [OPTIONS] < FILE
    COMMAND | org2term [OPTIONS]

OPTIONS:

    -w COLS     Max columns width.
    -m CHARS    Left margin.
    -h          Print this help and exit.
    -v          Print version and exit.

MARKUP:

    *TEXT*              Bold.
    /TEXT/              Italic.
    /*TEXT*/            Bold and italic.
    _TEXT_              Underlined.
    ~TEXT~              Inline code.
    =TEXT=              Verbatim (same as inline code).

    * TEXT              Header 1
    ** TEXT             Header 2
    *** TEXT            Header 3

    - TEXT              List item.

    #+begin_src         Code block.
    TEXT
    #+end_src

    #+begin_example     Preformatted block (same as code block).
    TEXT
    #+end_example

    #+begin_quote       Quote block.
    TEXT
    #+end_quote
"

copy="Org markup to escape sequences parser (org2term) $version

Copyright (C) 2022, Blau Araujo <blau@debxp.org>
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Blau Araujo.
"

# Styles ---------------------------------------------------------------------

# Disable all styles
reset=$'\e[0m'

# Colors
black=( $'\e[30m' $'\e[90m')
red=(   $'\e[31m' $'\e[91m')
green=( $'\e[32m' $'\e[92m')
yellow=($'\e[33m' $'\e[93m')
blue=(  $'\e[34m' $'\e[94m')
purple=($'\e[35m' $'\e[95m')
cyan=(  $'\e[36m' $'\e[96m')
grey=(  $'\e[37m' $'\e[97m')
orange=$'\e[38;5;208m'

# Text styles
b=($'\e[1m' $'\e[22m')
i=($'\e[3m' $'\e[23m')
u=($'\e[4m' $'\e[24m')
c=("$orange" "$reset")

# Default style
d=("${grey[1]}" "$reset")

# Blocks
h1="${b[0]}${blue[0]}"
h2="${b[0]}${green[1]}"
h3="${b[0]}${purple[1]}"
li="${blue[1]}"
pre="${yellow[1]}"
quote="${purple[1]}${i[0]}"

# Single line blocks ---------------------------------------------------------

# Headers
block_h1=('^\* +(.*)' "$h1\1$reset")
block_h2=('^\*\* +(.*)' "$h2\1$reset")
block_h3=('^\*\*\* +(.*)' "$h3\1$reset")

# List itens
block_li=('^- +(.*)' "$li• \1$reset")

# Inline styles --------------------------------------------------------------

bold=('\*\b([^*]+)\b\*' "${b[0]}\1${b[1]}")
italic=('/\b([^/]+)\b/' "${i[0]}\1${i[1]}")
code=('(~\b([^~]+)\b~|=\b([^=]+)\b=)' "${c[0]}\2\3${c[1]}")
underline=('\b_([^_]+)_\b' "${u[0]}\1${u[1]}")

# Messages -------------------------------------------------------------------

msg_err[0]="Try \`org2term -h' to get usage information."
msg_err[1]='Error: option -%s requires a value.'
msg_err[2]='Error: invalid option -%s.'
msg_err[3]='Error: wrong number of arguments.'

# Parameters -----------------------------------------------------------------

declare -a blocks

# Functions ------------------------------------------------------------------

die() {
    printf "${msg_err[$1]}\n" $2
    echo "${msg_err[0]}"
    exit $1
}

# Break text in blocks and make them into `blocks' elements...
split_blocks() {
    local i=0
    while IFS= read -r line || [[ "$line" ]]; do
        if [[ "${line%%_*}" = '#+begin' ]]; then
            blocks[i]+="$line"$'\n'
            until [[ "${line%%_*}" = '#+end' ]]; do
                read -r
                line="${REPLY//[[:space:]]/&nbsp;}"
                blocks[i]+="$line"$'\n'
            done
        elif [[ $line ]]; then
            blocks[i]+="$line"$'\n'
            until [[ -z $line ]]; do
                read -r line
                blocks[i]+="$line"$'\n'
            done
            blocks[i]="${blocks[i]::-1}"
        fi
        ((i++))
    done < $1
}

get_block_type() {
    local block="${blocks[$1]}"

    if [[ ${block%%_*} = '#+begin' ]]; then
        case ${block%%$'\n'*} in
              '#+begin_quote') echo quote;;
            '#+begin_example') echo pre;;
                '#+begin_src') echo pre;;
        esac
    else
        case ${block%% *} in
            '*'|'**'|'***') echo header;;
                         -) echo li;;
                         *) echo paragraph;;
        esac
    fi
}

block_parser() (

    header() {
        local level
        : "${1%% *}"
        level=${#_}
        s=block_h$level[0]
        r=block_h$level[1]
        sed -E "s:${!s}:${!r}:" <<< "${1^^}"
    }

    li() {
        s=block_li[0]
        r=block_li[1]
        sed -E "s:${!s}:${!r}:" <<< "$1"
    }

    pre() {
        printf "$pre"
        sed -Ee '/^#\+(begin|end)_(src|example).*/d' \
             -e 's/.*/│ &/' <<< "${1::-1}"
        echo "$reset"
    }

    quote() {
        printf "$quote"
        foldish $MAX_WIDTH <<< "${1::-1}" | \
        sed -Ee '/^#\+(begin|end)_quote.*/d' \
             -e "s/[\]{2}$//" \
             -e 's/.*/│ &/'
        echo "$reset"
    }

    paragraph() {
        foldish $MAX_WIDTH <<< "$1" | inline_parser
    }

    $1 "${2//&nbsp;/ }"
)

inline_parser() {
    sed -Eze "s:${code[0]}:${code[1]}:g"           \
          -e "s:${underline[0]}:${underline[1]}:g" \
          -e "s:${bold[0]}:${bold[1]}:g"           \
          -e "s:${italic[0]}:${italic[1]}:g"
}

# Main -----------------------------------------------------------------------

while getopts ':hvm:w:' opt; do
    case $opt in
        w)  [[ $OPTARG =~ ^[1-9][0-9]*$ ]] || {
                die 1 'w'
            }
            MAX_WIDTH=$OPTARG
            ;;
        m)  [[ $OPTARG =~ ^[1-9][0-9]*$ ]] || {
                die 1 'm'
            }
            MARGIN=$OPTARG
            printf -v SPC "%${MARGIN}s"
            ;;
        v)  echo "$copy"
            exit
            ;;
        h)  echo "$usage"
            exit
            ;;
        :)  die 1 "$OPTARG"
            ;;
        ?)  die 2 "$OPTARG"
            ;;
    esac
done
shift $((OPTIND-1))

[[ $MAX_WIDTH ]] || MAX_WIDTH=$(($(tput cols) - 2))
[[ $MARGIN ]] && (( MAX_WIDTH=MAX_WIDTH-MARGIN ))

if [[ -t 0 ]]; then
    [[ $# -eq 0 ]] && die 3
    split_blocks $1
else
    split_blocks /dev/stdin
fi

for n in "${!blocks[@]}"; do
   block_parser $(get_block_type $n) "${blocks[n]}" | \
   sed "s/.*/${SPC}&/"
done
